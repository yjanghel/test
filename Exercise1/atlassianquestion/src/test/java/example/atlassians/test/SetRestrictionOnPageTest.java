/*
 * SetRestrictionOnPageTest - Sample Test for setting up restriction on page
 */
package example.atlassians.test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import example.atlassian.helpmodules.OpenPageHelper;
import example.atlassian.pagerestrictions.RestrictionDialog;
import example.atlassian.pages.CreateBlankPage;
import example.atlassian.pages.Login;
import example.atlassian.pages.Page;
import example.atlassian.webdriverutils.WebDriverCommonUtils;

public class SetRestrictionOnPageTest {

	OpenPageHelper pageHelper;
	Login login;
	RestrictionDialog restrictionDialog;

	CreateBlankPage page;

	/** The xpath publish page button. */
	String XPATH_PUBLISH_PAGE_BUTTON = "//button[@id='rte-button-publish']";

	@BeforeTest
	public void setUp() {

		new WebDriverCommonUtils();

		login = new Login();

		pageHelper = new OpenPageHelper();

		restrictionDialog = new RestrictionDialog();

		page = new CreateBlankPage();

		login.openLoginPage();

		login.loginIntoPage();

	}

	@AfterTest
	public void tearDown() {

		WebDriverCommonUtils.logout();

		WebDriverCommonUtils.tearDown();
	}

	/**
	 * Test set restriction.
	 *
	 * @param userName  the user name
	 * @param pageTitle the page title
	 * @throws InterruptedException the interrupted exception
	 */
	@Parameters({ "userName", "pageTitle" })
	@Test
	public void testSetRestriction(String userName, String pageTitle) throws InterruptedException {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		pageHelper.searchPage(pageTitle);

		restrictionDialog.selectRestrictionOption("Editing restricted");

		restrictionDialog.addUserToEditRestricted(userName);

		restrictionDialog.removeUserFromEditRestriction(userName);

		restrictionDialog.clickOnApplyButton();

		page.publishPage();

	}

}
