/*
 * CreateDecisionPageTest - Sample Tests for Creating Decision Page
 */
package example.atlassians.test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import example.atlassian.pages.DecisionPage;
import example.atlassian.pages.Login;
import example.atlassian.webdriverutils.WebDriverCommonUtils;

public class CreateDecisionPageTest {
	
	DecisionPage decisionPage;
	
	Login login;
	
	
	@BeforeTest
	public void setUp() {
		
		new WebDriverCommonUtils();
		
		decisionPage = new DecisionPage();
		
		login = new Login();
		
		login.openLoginPage();
		
		login.loginIntoPage();
		
		WebDriverCommonUtils.clickCreatePageButton();
	}
	
	@AfterTest
	protected void tearDown(){
	
		WebDriverCommonUtils.logout();
		
		WebDriverCommonUtils.tearDown();
	}
	
	
	/**
	 * Test create decision page.
	 *
	 * @param spaceName the space name
	 * @param templateName the template name
	 * @param status the status
	 * @param pageTitle the page title
	 * @param owner the owner
	 * @param stakeHolder the stake holder
	 * @param strDate the str date
	 * @throws InterruptedException the interrupted exception
	 */
	@Parameters({ "spaceName", "templateName", "status", "pageTitle", "owner",
					"stakeHolder", "strDate"})
	@Test
	public void testCreateDecisionPage(String spaceName, String templateName,
										String status, String pageTitle,
										String owner, String stakeHolder, String strDate) throws InterruptedException {
		
		WebDriverCommonUtils.LOGGER.info(new Object() {}.getClass().getEnclosingMethod().getName());	
		
		decisionPage.setSpace(spaceName);
		
		decisionPage.clickShowMorePageButton();
		
		decisionPage.scrollAndSelectTheTemplate(templateName);
		
		decisionPage.clickNextButtonInCreateDialog();
		
		decisionPage.setStatus(status);
		
		decisionPage.setDecisionPageTitle(pageTitle);
		
		decisionPage.clearOwner();
		
		decisionPage.setOwner(owner);
		
		decisionPage.setStakeholders(stakeHolder);
		
		decisionPage.setDueDate(strDate);
		
		decisionPage.clickCreateCreatePageDialog();
		
		Thread.sleep(20000);
		
		decisionPage.publishPage();
		
	}
}
