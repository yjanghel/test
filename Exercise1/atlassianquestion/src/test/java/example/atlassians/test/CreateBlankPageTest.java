package example.atlassians.test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import example.atlassian.pages.CreateBlankPage;
import example.atlassian.pages.Login;
import example.atlassian.webdriverutils.WebDriverCommonUtils;

public class CreateBlankPageTest {

	CreateBlankPage blankPage;
	
	Login login;
	
	
	@BeforeTest
	public void setUp() {
		
		new WebDriverCommonUtils();
		
		blankPage = new CreateBlankPage();
		
		login = new Login();
		
		login.openLoginPage();
		
		login.loginIntoPage();
		
		WebDriverCommonUtils.clickCreatePageButton();
	}
	
	@AfterTest
	protected void tearDown(){
	
		WebDriverCommonUtils.logout();
		
		WebDriverCommonUtils.tearDown();
	}
	
	@Parameters({ "spaceName", "templateName", "pageTitle"})
	@Test
	public void testCreateBlankPageHappyPath(String spaceName, String templateName,
												String pageTitle) throws InterruptedException {
		
		WebDriverCommonUtils.LOGGER.info(new Object() {}.getClass().getEnclosingMethod().getName());
		
		blankPage.setSpace(spaceName);
		
		blankPage.clickShowMorePageButton();
		
		blankPage.scrollAndSelectTheTemplate(templateName);
		
		blankPage.clickCreateCreatePageDialog();
		
		Thread.sleep(10000);
		
		blankPage.setPageTitle(pageTitle);
		
		blankPage.publishPage();
		
	}
	
}
