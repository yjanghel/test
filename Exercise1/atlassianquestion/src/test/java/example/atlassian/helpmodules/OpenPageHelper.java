/*
 * OpenPageHelper.java  - Contains Utility Functions which we need to run tests end to end.
 */
package example.atlassian.helpmodules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


import example.atlassian.webdriverutils.WebDriverCommonUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenPageHelper.
 */
public class OpenPageHelper {

	/** The xpath search button. */
	String XPATH_SEARCH_BUTTON = "//div[@class='global-search-icon']";
	
	/** The xpath search text box. */
	String XPATH_SEARCH_TEXT_BOX = "//input[@placeholder='Search Confluence']";
	
	/** The page name. */
	String PAGE_NAME = "Tech Doc 1";
	
	/** The xpath page name href. */
	String XPATH_PAGE_NAME_HREF = "(//a[contains(@href,'Tech+Doc+1')])[1]";
	
	/** The id edit page button. */
	String ID_EDIT_PAGE_BUTTON = "editPageLink";
	
	/** The xpath enable restrictions button. */
	String XPATH_ENABLE_RESTRICTIONS_BUTTON = "//a[@id='rte-button-restrictions']";

	/** The xpath advanced search label. */
	String XPATH_ADVANCED_SEARCH_LABEL = "//span[@aria-label=\"Advanced search\"]";
	
	/** The xpath search page text box. */
	String XPATH_SEARCH_PAGE_TEXT_BOX = "//input[@id='query-string']";
	
	/** The xpath search in space. */
	String XPATH_SEARCH_IN_SPACE = "//div[@id=\"s2id_cql-field-space-1\"]/ul/li/input";
	
	/** The xpath click on search button advanced page. */
	String XPATH_CLICK_ON_SEARCH_BUTTON_ADVANCED_PAGE = "//button[@type='submit']";
	
	/**
	 * Search page.
	 *
	 * @param pageName the page name
	 * @throws InterruptedException the interrupted exception
	 */
	public void searchPage(String pageName) throws InterruptedException {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.clickElementByXPath(XPATH_SEARCH_BUTTON);
		
		Thread.sleep(5000);
		
		WebDriverCommonUtils.clickElementByXPath(XPATH_ADVANCED_SEARCH_LABEL);
		
		WebDriverCommonUtils.sendKeysToElementByXPath(XPATH_SEARCH_PAGE_TEXT_BOX, PAGE_NAME);
		
		WebDriverCommonUtils.clickElementByXPath(XPATH_CLICK_ON_SEARCH_BUTTON_ADVANCED_PAGE);
		
		WebDriverCommonUtils.clickElementByXPath("//a[contains(@href, '" + PAGE_NAME.replaceAll(" ", "+") + "')]");
		
		WebDriverCommonUtils.clickElementById(ID_EDIT_PAGE_BUTTON);
		
		Thread.sleep(10000);
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_ENABLE_RESTRICTIONS_BUTTON);
		
		WebElement button = WebDriverCommonUtils.eFiring.findElement(By.xpath(XPATH_ENABLE_RESTRICTIONS_BUTTON)); 

	    Actions build = new Actions(WebDriverCommonUtils.eFiring); 
	    
	    build.moveToElement(button).build().perform(); 
	    
	    WebElement clickButton= WebDriverCommonUtils.eFiring.findElement(By.xpath(XPATH_ENABLE_RESTRICTIONS_BUTTON));
	    
	    clickButton.click();
		
	}
}
