/*
 * DecisionPage.java - Contains functions to create Decision Page
 */
package example.atlassian.pages;

import example.atlassian.webdriverutils.WebDriverCommonUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class DecisionPage.
 */
public class DecisionPage extends Page {
	
	/** The id set decision drop down. */
	String ID_SET_DECISION_DROP_DOWN = "decisions-status";
	
	/** The id set decision page title. */
	String ID_SET_DECISION_PAGE_TITLE = "decisions-page-title";
	
	/** The xpath set owner text box. */
	String XPATH_SET_OWNER_TEXT_BOX = "(//li[@class=\"select2-search-field\"]//input)[1]";
	
	/** The xpath set stakeholders text box. */
	String XPATH_SET_STAKEHOLDERS_TEXT_BOX = "(//li[@class=\"select2-search-field\"]//input)[2]";
	
	/** The id set date text box. */
	String ID_SET_DATE_TEXT_BOX = "decisions-due-date";
	
	/** The xpath create button create dialog. */
	String XPATH_CREATE_BUTTON_CREATE_DIALOG = "(//button[@class='create-dialog-create-button aui-button aui-button-primary'])[2]";
	
	/** The xpath publish page button. */
	String XPATH_PUBLISH_PAGE_BUTTON = "//button[@id='rte-button-publish']";
	
	
	/* (non-Javadoc)
	 * @see example.atlassian.pages.Page#scrollAndSelectTheTemplate(java.lang.String)
	 */
	public void scrollAndSelectTheTemplate(String templateName) {

		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		super.scrollAndSelectTheTemplate(templateName);
	}

	/* (non-Javadoc)
	 * @see example.atlassian.pages.Page#setSpace(java.lang.String)
	 */
	public void setSpace(String spaceName) {

		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		super.setSpace(spaceName);
	}
	
	/**
	 * Publish page.
	 */
	public void publishPage() {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());

		super.publishPage(XPATH_PUBLISH_PAGE_BUTTON);
	}

	/* (non-Javadoc)
	 * @see example.atlassian.pages.Page#clickShowMorePageButton()
	 */
	public void clickShowMorePageButton() {

		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		super.clickShowMorePageButton();

	}	
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.selectFromDropDownById(ID_SET_DECISION_DROP_DOWN, status);
	}
	
	/**
	 * Sets the decision page title.
	 *
	 * @param decision the new decision page title
	 */
	public void setDecisionPageTitle(String decision) {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.sendKeysToElementById(ID_SET_DECISION_PAGE_TITLE, decision);
	}
	
	
	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(String owner) {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.mimicUserInputOfString(XPATH_SET_OWNER_TEXT_BOX, owner);
		
		WebDriverCommonUtils.waitForElementByXPath("//div[@id=\"select2-drop\"]//span[@data-username='" + owner + "']");
		
		WebDriverCommonUtils.clickElementByXPath("//div[@id=\"select2-drop\"]//span[@data-username='" + owner + "']");
		
	}

	
	/**
	 * Clear owner.
	 */
	public void clearOwner() {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.js.executeScript("document.getElementsByClassName(\"select2-search-choice-close\")[1].click();");
	}
	
	/**
	 * Sets the stakeholders.
	 *
	 * @param name the new stakeholders
	 */
	public void setStakeholders(String name) {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.mimicUserInputOfString(XPATH_SET_STAKEHOLDERS_TEXT_BOX, name);
		
		WebDriverCommonUtils.waitForElementByXPath("//div[@id=\"select2-drop\"]//span[@data-username='" + name + "']");
		
		WebDriverCommonUtils.clickElementByXPath("//div[@id=\"select2-drop\"]//span[@data-username='" + name + "']");
		
	}
	
	/**
	 * Sets the due date.
	 *
	 * @param strDate the new due date
	 */
	public void setDueDate(String strDate) {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.sendKeysToElementById(ID_SET_DATE_TEXT_BOX, strDate);
		
		WebDriverCommonUtils.sendTabKeysToId(ID_SET_DATE_TEXT_BOX);
	}
	
	/* (non-Javadoc)
	 * @see example.atlassian.pages.Page#clickCreateCreatePageDialog()
	 */
	public void clickCreateCreatePageDialog() {

		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.clickElementByXPath(XPATH_CREATE_BUTTON_CREATE_DIALOG);

	}
	
}
