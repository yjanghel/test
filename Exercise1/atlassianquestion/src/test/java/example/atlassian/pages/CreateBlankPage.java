/*
 * CreateBlankPage.java - Creates a blank page
 */
package example.atlassian.pages;

import example.atlassian.webdriverutils.WebDriverCommonUtils;

public class CreateBlankPage extends Page {

	/** The xpath publish page button. */
	String XPATH_PUBLISH_PAGE_BUTTON = "//button[@id='publish-button']";
	
	public void setPageTitle(String pageTitle) {

		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		super.setPageTitle(pageTitle);

	}

	public void publishPage() {

		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		super.publishPage(XPATH_PUBLISH_PAGE_BUTTON);
	}

	public void clickShowMorePageButton() {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		super.clickShowMorePageButton();

	}

	public void setSpace(String spaceName) {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());

		super.setSpace(spaceName);
	}

	public void scrollAndSelectTheTemplate(String templateName) {

		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		super.scrollAndSelectTheTemplate(templateName);
	}

	public void clickCreateCreatePageDialog() {

		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		super.clickCreateCreatePageDialog();

	}
}
