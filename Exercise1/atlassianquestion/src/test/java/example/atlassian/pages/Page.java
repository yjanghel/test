/*
 * Page.java - This file contains generic Page helper modules. If any new generic page functionality gets added
 * , we should add corresponding Selenium steps in here.
 * This Class should should be extended by New Page Templates or Existing Page Templates, as each page template
 * requires specific information.
 */
package example.atlassian.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import example.atlassian.webdriverutils.WebDriverCommonUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class Page.
 */
public class Page {

	/** The xpath set page title. */
	String XPATH_SET_PAGE_TITLE = "//textarea[@data-test-id=\"editor-title\"]";

	/** The xpath close button create dialog. */
	String XPATH_CLOSE_BUTTON_CREATE_DIALOG = "(//a[@class='button-panel-link button-panel-cancel-link'])[2]";

	/** The xpath create button create dialog. */
	String XPATH_CREATE_BUTTON_CREATE_DIALOG = "//button[@class='create-dialog-create-button aui-button aui-button-primary']";

	/** The id show more link. */
	String ID_SHOW_MORE_LINK = "promoted-link";

	/** The xpath space drop down. */
	String XPATH_SPACE_DROP_DOWN = "//div[@class='space-select-control-container']";

	/** The xpath space text box. */
	String XPATH_SPACE_TEXT_BOX = "//*[@id=\"select2-drop\"]//input";

	/** The xpath next button in create page dialog. */
	String XPATH_NEXT_BUTTON_IN_CREATE_PAGE_DIALOG = "//button[@data-test-id=\"create-dialog-create-button\"]";

	/** The xpath back button in create page dialog. */
	String XPATH_BACK_BUTTON_IN_CREATE_PAGE_DIALOG = "//button[@class='button-panel-back aui-button']";

	/** The Id for Filter Text Box. */
	String ID_FILTER_TEXT_BOX = "createDialogFilter";

	/** The xpath search button. */
	String XPATH_SEARCH_BUTTON = "//div[@class='global-search-icon']";

	/** The xpath search text box. */
	String XPATH_SEARCH_TEXT_BOX = "//input[@placeholder='Search Confluence']";

	/** The xpath help link. */
	String XPATH_HELP_LINK = "//div[@class='dialog-help-link']";

	/** The xpath help href. */
	String XPATH_HELP_HREF = "//a[@href='https://confluence.atlassian.com/display/ConfCloud/Pages+and+Blogs']";

	/** The xpath customized link. */
	String XPATH_CUSTOMIZED_LINK = "//a[@class='add-remove-customise-templates-trigger button-panel-link']";

	/**
	 * Sets the space.
	 *
	 * @param space the new space
	 */
	public void setSpace(String space) {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.clickElementByXPath(XPATH_SPACE_DROP_DOWN);

		WebDriverCommonUtils.sendKeysToElementByXPath(XPATH_SPACE_TEXT_BOX, space);

		WebDriverCommonUtils.eFiring.findElement(By.xpath(XPATH_SPACE_TEXT_BOX)).sendKeys(Keys.ENTER);

	}

	/**
	 * Click close create page dialog.
	 */
	public void clickCloseCreatePageDialog() {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.clickElementByXPath(XPATH_CLOSE_BUTTON_CREATE_DIALOG);

	}

	/**
	 * Click create create page dialog.
	 */
	public void clickCreateCreatePageDialog() {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.clickElementByXPath(XPATH_CREATE_BUTTON_CREATE_DIALOG);
	}

	/**
	 * Click show more page button.
	 */
	public void clickShowMorePageButton() {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.clickElementtByIdNoFailure(ID_SHOW_MORE_LINK);
	}

	/**
	 * Scroll and select the page template.
	 *
	 * @param templateName the page template name
	 */
	public void scrollAndSelectTheTemplate(String templateName) {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebElement e = WebDriverCommonUtils.eFiring
				.findElement(By.xpath("//div[contains(text(),'" + templateName + "')]"));

		WebDriverCommonUtils.eFiring.executeScript("arguments[0].scrollIntoView();", e);

		e.click();
	}

	/**
	 * Click next button in create dialog.
	 */
	public void clickNextButtonInCreateDialog() {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.clickElementByXPath(XPATH_NEXT_BUTTON_IN_CREATE_PAGE_DIALOG);

	}

	/**
	 * Click back button in create dialog.
	 */
	public void clickBackButtonInCreateDialog() {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.clickElementByXPath(XPATH_BACK_BUTTON_IN_CREATE_PAGE_DIALOG);

	}

	/**
	 * Filter Page Template.
	 *
	 * @param templateName the template name
	 */
	public void filterPageTemplate(String templateName) {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.waitForElementById(ID_FILTER_TEXT_BOX);

		WebDriverCommonUtils.sendKeysToElementById(ID_FILTER_TEXT_BOX, templateName);

	}

	/**
	 * Click on Help link.
	 */
	public void checkHelpLink() {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.waitForElementByXPath(XPATH_HELP_LINK);
		WebDriverCommonUtils.waitForElementByXPath(XPATH_HELP_HREF);

	}

	/**
	 * Customize Template Help Link.
	 */
	public void checkCustomizeTemplateLink() {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.waitForElementByXPath(XPATH_CUSTOMIZED_LINK);
	}

	/**
	 * Check if parent Page is showing up in create dialog.
	 *
	 * @param parentPage the parent page
	 */
	public void checkParentPage(String parentPage) {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.waitForElementByXPath(
				"//div[@id=\"create-dialog-parent-container\"]/span[contains(text()," + parentPage + ")]");
	}

	/**
	 * Check if page got created properly.
	 *
	 * @param pageTitle the page title
	 */
	public void checkPageGotCreated(String pageTitle) {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.clickElementByXPath(XPATH_SEARCH_BUTTON);

		WebDriverCommonUtils.sendKeysToElementByXPath(XPATH_SEARCH_TEXT_BOX, pageTitle);

		WebDriverCommonUtils.clickElementByXPath("//span[contains(text()," + pageTitle + ")]");

	}

	/**
	 * Sets the page title.
	 *
	 * @param title the new page title
	 */
	public void setPageTitle(String title) {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.sendKeysToElementByXPath(XPATH_SET_PAGE_TITLE, title);
	}

	/**
	 * Publish page.
	 */
	public void publishPage(String xpathOfPublishButton) {

		WebDriverCommonUtils.LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.clickElementByXPath(xpathOfPublishButton);
	}

}
