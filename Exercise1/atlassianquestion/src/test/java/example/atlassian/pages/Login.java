/*
 * Login.java - Helper code, just to check the whole flow of test cases
 */
package example.atlassian.pages;


import example.atlassian.webdriverutils.*;


/**
 * The Class Login.
 */
public class Login {

	/** The id login text box. */
	String ID_LOGIN_TEXT_BOX = "username";
	
	/** The id login password text box. */
	String ID_LOGIN_PASSWORD_TEXT_BOX = "password";
	
	/** The id continue. */
	String ID_CONTINUE = "login-submit";
	
	
	/**
	 * Open login page.
	 */
	public void openLoginPage() {
		
		WebDriverCommonUtils.openLoginUrl();
		
		WebDriverCommonUtils.waitForElementById(ID_LOGIN_TEXT_BOX);		
		
	}
	
	/**
	 * Login into page.
	 */
	public void loginIntoPage() {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.sendKeysToElementById(ID_LOGIN_TEXT_BOX, WebDriverCommonUtils.LOGIN_USERNAME);
		
		WebDriverCommonUtils.clickElementById(ID_CONTINUE);
		
		WebDriverCommonUtils.sendKeysToElementById(ID_LOGIN_PASSWORD_TEXT_BOX, WebDriverCommonUtils.LOGIN_PASSWORD);
		
		WebDriverCommonUtils.clickElementById(ID_CONTINUE);			
	}

}
