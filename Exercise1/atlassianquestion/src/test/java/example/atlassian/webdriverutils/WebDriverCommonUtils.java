/*
 * WebDriverCommonUtils.java - File contains all utility functions
 * 
 */
package example.atlassian.webdriverutils;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * The Class WebDriverCommonUtils - Contains Utility Function
 */
public class WebDriverCommonUtils {

	/** The id create page button. */
	static String ID_CREATE_PAGE_BUTTON = "create-page-button";

	/** The xpath user logout button. */
	static String XPATH_USER_LOGOUT_BUTTON = "//div[@data-webitem-location='system.user']";

	/** The xpath log out text. */
	static String XPATH_LOG_OUT_TEXT = "//span[.='Log Out']";

	/** The id logout submit button. */
	static String ID_LOGOUT_SUBMIT_BUTTON = "logout-submit";

	/** The driver. */
	public static WebDriver driver;

	/** The e firing. */
	public static EventFiringWebDriver eFiring;

	/** The e listener. */
	public static WebEventListener eListener;

	/** The js. */
	public static JavascriptExecutor js;

	/** The wait. */
	public static WebDriverWait wait;

	/** The properties. */
	public static Properties properties;

	/** The login url. */
	public static String LOGIN_URL;

	/** The login username. */
	public static String LOGIN_USERNAME;

	/** The login password. */
	public static String LOGIN_PASSWORD;

	/** The Constant LOGGER. */
	public static final Logger LOGGER = Logger.getLogger(WebDriverCommonUtils.class);;

	/**
	 * Instantiates a new web driver common utils.
	 */
	public WebDriverCommonUtils() {

		properties = new Properties();
		try {
			properties.load(new FileReader(new File("src/test/resources/atlassian.properties")));
		} catch (IOException e) {

			LOGGER.error(e.toString());
		}

		/*
		 * Get and Set Variables from the resource file
		 */
		LOGIN_URL = properties.getProperty("atlassian.login.url");
		LOGIN_USERNAME = properties.getProperty("atlassian.login.email");
		LOGIN_PASSWORD = properties.getProperty("atlassian.login.password");

		System.setProperty("webdriver.chrome.driver", properties.getProperty("webdriver.chrome.driver"));

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);

		eFiring = new EventFiringWebDriver(driver);

		eListener = new WebEventListener();

		eFiring.register(eListener);

		wait = new WebDriverWait(eFiring, 40);

		js = (JavascriptExecutor) eFiring;

	}

	/**
	 * Open login url.
	 * 
	 */
	public static void openLoginUrl() {

		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		eFiring.get(LOGIN_URL);
	}

	/**
	 * Wait for element by id.
	 *
	 * @param elementId the element id
	 */
	public static void waitForElementById(String elementId) {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id(elementId)));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(elementId)));
		} catch (NotFoundException e) {
			LOGGER.error(e.toString());
			fail();
		} catch (Exception e) {
			LOGGER.error(e.toString());
			fail();
		}

	}

	/**
	 * Wait for element by X path.
	 *
	 * @param xpath the xpath
	 */
	public static void waitForElementByXPath(String xpath) {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		try {
		
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		
		} catch (NotFoundException e) {
		
			LOGGER.error(e.toString());
			
			fail();
		
		} catch (Exception e) {
		
			LOGGER.error(e.toString());
			
			fail();
		}

	}

	/**
	 * Send keys to element by id.
	 *
	 * @param elementId the element id
	 * @param str       the str
	 */
	public static void sendKeysToElementById(String elementId, String str) {
		
		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		waitForElementById(elementId);

		try {
		
			eFiring.findElement(By.id(elementId)).sendKeys(str);
		
		} catch (Exception e) {
		
			LOGGER.error(e.toString());
			
			fail();
		}

	}

	/**
	 * Send keys to element by X path.
	 *
	 * @param xpath the xpath
	 * @param str   the str
	 */
	public static void sendKeysToElementByXPath(String xpath, String str) {
		
		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		waitForElementByXPath(xpath);

		try {
		
			eFiring.findElement(By.xpath(xpath)).sendKeys(str);
	
		} catch (Exception e) {
		
			LOGGER.error(e.toString());
			
			fail();
		}

	}

	/**
	 * Click element by id.
	 *
	 * @param elementId the element id
	 */
	public static void clickElementById(String elementId) {
		
		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		waitForElementById(elementId);

		try {
		
			wait.until(ExpectedConditions.elementToBeClickable(By.id(elementId))).click();
		
		} catch (ElementClickInterceptedException e) {
			
			LOGGER.error(e.toString());
			
			fail();
		
		} catch (Exception e) {
		
			LOGGER.error(e.toString());
			
			fail();
		}
	}

	/**
	 * Click element by X path.
	 *
	 * @param xpath the xpath
	 */
	public static void clickElementByXPath(String xpath) {
		
		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		waitForElementByXPath(xpath);
		
		try {
		
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).click();
			
		} catch (Exception e) {
			
			LOGGER.error(e.toString());
			
			fail();
			
		}
		
	}

	/**
	 * Click elementt by id no failure.
	 *
	 * @param elementId the element id
	 */
	public static void clickElementtByIdNoFailure(String elementId) {
		
		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		
		try {
		
			wait.until(ExpectedConditions.elementToBeClickable(By.id(elementId))).click();
		
		} catch (ElementClickInterceptedException e) {
		
			LOGGER.error(e.toString());
		
		} catch (Exception e) {
		
			LOGGER.error(e.toString());
		
		}
	}

	/**
	 * Select from drop down by id.
	 *
	 * @param dropDownId the drop down id
	 * @param item       the item
	 */
	public static void selectFromDropDownById(String dropDownId, String item) {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		waitForElementById(dropDownId);

		Select dropDown = new Select(eFiring.findElement(By.id(dropDownId)));
		
		dropDown.selectByVisibleText(item);
	}

	/**
	 * Clear text box by id.
	 *
	 * @param textBoxId the text box id
	 */
	public static void clearTextBoxById(String textBoxId) {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		waitForElementById(textBoxId);

		eFiring.findElement(By.id(textBoxId)).clear();

	}

	/**
	 * Clear text box by X path.
	 *
	 * @param textBoxId the text box id
	 */
	public static void clearTextBoxByXPath(String textBoxId) {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		waitForElementByXPath(textBoxId);

		eFiring.findElement(By.xpath(textBoxId)).clear();

	}

	/**
	 * Back space text box by X path.
	 *
	 * @param textBoxId the text box id
	 */
	public static void backSpaceTextBoxByXPath(String textBoxId) {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		waitForElementByXPath(textBoxId);

		eFiring.findElement(By.xpath(textBoxId)).clear();;

	}

	/**
	 * Send enter keys to id.
	 *
	 * @param elementId the element id
	 */
	public static void sendEnterKeysToId(String elementId) {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		waitForElementById(elementId);

		eFiring.findElement(By.id(elementId)).sendKeys(Keys.ENTER);
	}
	
	/**
	 * Send enter keys to id.
	 *
	 * @param elementId the element id
	 */
	public static void sendTabKeysToId(String elementId) {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		waitForElementById(elementId);

		eFiring.findElement(By.id(elementId)).sendKeys(Keys.TAB);
	}

	/**
	 * Send enter keys to X path.
	 *
	 * @param elementId the element id
	 */
	public static void sendEnterKeysToXPath(String elementId) {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		waitForElementByXPath(elementId);

		eFiring.findElement(By.id(elementId)).sendKeys(Keys.ENTER);
	}

	/**
	 * Click create page button.
	 */
	public static void clickCreatePageButton() {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.clickElementById(ID_CREATE_PAGE_BUTTON);
	}

	/**
	 * Logout.
	 */
	public static void logout() {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriverCommonUtils.clickElementByXPath(XPATH_USER_LOGOUT_BUTTON);

		WebDriverCommonUtils.clickElementByXPath(XPATH_LOG_OUT_TEXT);

		WebDriverCommonUtils.clickElementById(ID_LOGOUT_SUBMIT_BUTTON);
	}
	
	public static void mimicUserInputOfString(String xpath, String str) {
		for(int i = 0; i < str.length(); i++) {
		        try {
		            Thread.sleep(1000);
		        } catch(InterruptedException e) {}
		        String s = new StringBuilder().append(str.charAt(i)).toString();
		        WebDriverCommonUtils.sendKeysToElementByXPath(xpath, s);
		    }
	}

	/**
	 * Tear down.
	 */
	public static void tearDown() {

		LOGGER.info(new Object() {
		}.getClass().getEnclosingMethod().getName());

		eFiring.quit();
	}
}
