/*
 * RestrictionDialog.java - Contains Utility Function which provides functionality of Restriction
 * Dialog Pop Up. 
 */
package example.atlassian.pagerestrictions;

import example.atlassian.webdriverutils.WebDriverCommonUtils;


// TODO: Auto-generated Javadoc
/**
* The Class RestrictionDialog.
*/
public class RestrictionDialog {

	/** The xpath page restriction dialog selector. */
	String XPATH_PAGE_RESTRICTION_DIALOG_SELECTOR = "//*[@id=\"s2id_page-restrictions-dialog-selector\"]";
	
	/** The xpath page restriction edit restricted option. */
	String XPATH_PAGE_RESTRICTION_EDIT_RESTRICTED_OPTION ="//span[@class='title'][contains(text(),'Editing restricted')]";
	
	/** The xpath page restriction no restriction option. */
	String XPATH_PAGE_RESTRICTION_NO_RESTRICTION_OPTION ="//span[@class='title'][contains(text(),'No restriction')]";
	
	/** The xpath page restriction view edit restricted option. */
	String XPATH_PAGE_RESTRICTION_VIEW_EDIT_RESTRICTED_OPTION ="//span[@class='title'][contains(text(),'Viewing and editing restricted')]";
	
	/** The xpath page restriction edit restricted option user text box. */
	String XPATH_PAGE_RESTRICTION_EDIT_RESTRICTED_OPTION_USER_TEXT_BOX = "//div[@id='s2id_restrictions-dialog-auto-picker']//input";
	
	/** The xpath page restriction add button. */
	String XPATH_PAGE_RESTRICTION_ADD_BUTTON = "//button[@id='page-restrictions-add-button']";
	
	/** The xpath help href. */
	String XPATH_HELP_HREF = "//a[@href='https://confluence.atlassian.com/display/ConfCloud/Add+or+Remove+Page+Restrictions']";
	
	/** The id apply buttton. */
	String ID_APPLY_BUTTTON = "page-restrictions-dialog-save-button";
	
	/** The xpath label restriction. */
	String XPATH_LABEL_RESTRICTION = "//h2[@class='aui-dialog2-header-main'][contains(text(),'Restrictions')]";
	
	/**
	 * Gets the all restriction options from drop down.
	 *
	 * @return the all restriction options from drop down
	 */
	public void checkAllRestrictionOptionsFromDropDown() {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_PAGE_RESTRICTION_DIALOG_SELECTOR);
		
		WebDriverCommonUtils.clickElementByXPath(XPATH_PAGE_RESTRICTION_DIALOG_SELECTOR);
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_PAGE_RESTRICTION_EDIT_RESTRICTED_OPTION);
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_PAGE_RESTRICTION_VIEW_EDIT_RESTRICTED_OPTION);
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_PAGE_RESTRICTION_NO_RESTRICTION_OPTION);
		
	}
	
	
	/**
	 * Select restriction option.
	 *
	 * @param restriction the restriction
	 * @throws InterruptedException the interrupted exception
	 */
	public void selectRestrictionOption(String restriction) throws InterruptedException {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_PAGE_RESTRICTION_DIALOG_SELECTOR);
		
		WebDriverCommonUtils.clickElementByXPath(XPATH_PAGE_RESTRICTION_DIALOG_SELECTOR);
		
		Thread.sleep(2000);
		
		WebDriverCommonUtils.clickElementByXPath("//span[@class='title'][contains(text(),'" + restriction + "')]");
		
	}
	
	
	/**
	 * Adds the user to edit restricted.
	 *
	 * @param user the user
	 */
	public void addUserToEditRestricted(String user) {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_PAGE_RESTRICTION_EDIT_RESTRICTED_OPTION_USER_TEXT_BOX);
		
		WebDriverCommonUtils.mimicUserInputOfString(XPATH_PAGE_RESTRICTION_EDIT_RESTRICTED_OPTION_USER_TEXT_BOX, user);
		
		WebDriverCommonUtils.waitForElementByXPath("//div[@id=\"select2-drop\"]//span[@data-username='" + user + "']");
		
		WebDriverCommonUtils.clickElementByXPath("//div[@id=\"select2-drop\"]//span[@data-username=\"" + user + "\"]");
		
		WebDriverCommonUtils.clickElementByXPath(XPATH_PAGE_RESTRICTION_ADD_BUTTON);
		
	}
	
	/**
	 * Removes the user from edit restriction.
	 *
	 * @param user the user
	 */
	public void removeUserFromEditRestriction(String user) {
		
		WebDriverCommonUtils.LOGGER.info(new Object(){}.getClass().getEnclosingMethod().getName());
		
		WebDriverCommonUtils.clickElementByXPath("//tr[@data-full-name='" + user + "']/td/button[text()=\"Remove\"]");
		
	}
	
	/**
	 * Adds the user to view and edit restricted.
	 *
	 * @param user the user
	 */
	public void addUserToViewAndEditRestricted(String user) {
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_PAGE_RESTRICTION_EDIT_RESTRICTED_OPTION_USER_TEXT_BOX);
		
		WebDriverCommonUtils.sendKeysToElementByXPath(XPATH_PAGE_RESTRICTION_EDIT_RESTRICTED_OPTION_USER_TEXT_BOX, user);
		
		WebDriverCommonUtils.waitForElementByXPath("//div[@id=\"select2-drop\"]//span[@data-username=\"" + user + "\"]");
		
		WebDriverCommonUtils.clickElementByXPath("//div[@id=\"select2-drop\"]//span[@data-username=\"" + user + "\"]");
		
		WebDriverCommonUtils.clickElementByXPath(XPATH_PAGE_RESTRICTION_ADD_BUTTON);
	
	}
	
	/**
	 * Check help link is present.
	 */
	public void checkHelpLinkIsPresent() {
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_HELP_HREF);
	}
	
	/**
	 * Click on apply button.
	 */
	public void clickOnApplyButton() {
		
		WebDriverCommonUtils.waitForElementById(ID_APPLY_BUTTTON);
		WebDriverCommonUtils.clickElementById(ID_APPLY_BUTTTON);
		
	}
	
	/**
	 * Check page label restriction.
	 */
	public void checkPageLabelRestriction() {
		
		WebDriverCommonUtils.waitForElementByXPath(XPATH_LABEL_RESTRICTION);
	}
}
